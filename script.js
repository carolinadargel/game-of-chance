// // Magic Ball



function getRandom(max) {
    return Math.floor(Math.random() * max + 1)
}


function mensagem(a) {

    var exibemsg = document.getElementById("mensagem");

    const expr = 'x';
    switch (a) {

        case 1:
            exibemsg.textContent = "Só existem dois dias no ano que nada pode ser feito. Um se chama ONTEM e o outro se chama AMANHÃ, portanto HOJE é o dia certo para amar, acreditar, fazer e principalmente viver.";
            break;
        case 2:
            exibemsg.textContent = "É muito melhor perceber um defeito em sim mesmo, do que dezenas no outro, pois o seu defeito você pode mudar.";
            break;

        case 3:
            exibemsg.textContent = "Neste mundo não existe verdade universal. Uma mesma verdade pode apresentar diferentes fisionomias. Tudo depende das decifrações feitas através de nossos prismas intelectuais, filosóficos, culturais e religiosos.";
            break;

        case 4:
            exibemsg.textContent = "As criaturas que habitam esta terra em que vivemos, sejam elas seres humanos ou animais, estão aqui para contribuir, cada uma com sua maneira peculiar, para a beleza e a prosperidade do mundo.";
            break;

        case 5:
            exibemsg.textContent = "A raiva não pode ser superada pela raiva. Se uma pessoa demonstra raiva de você, e você mostrar raiva em troca, o resultado é um desastre.";
            break;

        case 6:
            exibemsg.textContent = "Se a criança não receber a devida atenção, em geral, quando adulta, tem dificuldade de amar seus semelhantes.";
            break;

        case 7:
            exibemsg.textContent = "O que é meu inimigo? Eu mesmo. Minha ignorância, meu apego, meus ódios. Aí está meu inimigo...";
            break;

        case 8:
            exibemsg.textContent = "Lar é onde você se sente em casa e é bem tratado.";
            break;

        case 9:
            exibemsg.textContent = "Se você quer transformar o mundo, experimente primeiro promover o seu aperfeiçoamento pessoal e realizar inovações no seu próprio interior. Estas atitudes se refletirão em mudanças positivas no seu ambiente familiar. Deste ponto em diante, as mudanças se expandirão em proporções cada vez maiores. Tudo o que fazemos produz efeito, causa algum impacto.";
            break;

        case 10:
            exibemsg.textContent = "A mais profunda raiz do fracasso em nossas vidas é pensar, 'Como sou inútil e fraco'. É essencial pensar poderosa e firmemente, 'Eu consigo', sem ostentação ou preocupação.";
            break;

        case 11:
            exibemsg.textContent = "O período de maior ganho em conhecimento e experiência é o período mais difícil da vida de alguém.";
            break;

        case 12:
            exibemsg.textContent = "A mais profunda raiz do fracasso em nossas vidas é pensar, 'Como sou inútil e fraco'. É essencial pensar poderosa e firmemente, 'Eu consigo', sem ostentação ou preocupação.";
            break;

        case 13:
            exibemsg.textContent = "O período de maior ganho em conhecimento e experiência é o período mais difícil da vida de alguém.";

            break;

        default:
            (expr);

    }
}